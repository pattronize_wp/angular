 var mainApp = angular.module("mainApp", ["xeditable"]);
mainApp.run(function(editableOptions) {
  editableOptions.theme = 'bs3';
});