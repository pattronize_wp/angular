angular.module('mainApp')
	.controller('FruitsControl', function FruitsControlCallback($scope,$filter){
        $scope.name="Anonymous"
        $scope.fruits=[ 
			{value: 'apple', status: 'instock'},
			{value: 'banana', status: 'instock'},
			{value: 'orange', status: 'instock'},
			{value: 'Guava', status: 'outstock'}
		];
        $scope.addFruit=function(){
            $scope.fruits.push({value:$scope.newFruit, status: 'instock'});
        }
		$scope.removeFruit= function(index){
    		$scope.fruits.splice(index, 1);
  		}
		
    });