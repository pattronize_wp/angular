<?php
class Father {
	protected $a =10;
    protected function myMethod() {
        return "hello";
    } 
	static{
		echo "My static block";
	}
	public function __construct(){
		echo "My constructor block";
	}
	
}

class Child extends Father {
	protected $a =30;
    public function myMethod() {
        return "hi";
    }
}

class GrandChild extends Child {
    public function myOtherMethod() {
        echo parent::myMethod(), "\n"; // Child::myMethod
        echo $this->myMethod(), "\n";  // Child::myMethod
		echo $this->a;
    }
}

$obj = new GrandChild;
$obj->myOtherMethod();
?>