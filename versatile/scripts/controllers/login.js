'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('LoginCtrl', function($scope, $cookieStore,$location,$http) {
	$scope.errors = [];
	$scope.msgs = [];
    $scope.submit_form = function(isValid) {
    	if(isValid){
			$scope.errors.splice(0, $scope.errors.length); // remove all error messages
			$scope.msgs.splice(0, $scope.msgs.length);
			$http.post('backend/check_login.php', {'uname': $scope.username, 'pswd': $scope.password})
				.success(function(data, status, headers, config) {
	                        if (data.msg != '')
	                        {
								$scope.msgs.push(data.msg);
								$cookieStore.put('username', $scope.username);
								$location.path('/dashboard');
	                        }
	                        else
	                        {
								$scope.errors.push(data.error);
	                        }
	                    })
				.error(function(data, status) { // called asynchronously if an error occurs
							// or server returns response with an error status.
	                        $scope.errors.push(status);
	        })
			return false;
    	}
	}
});
