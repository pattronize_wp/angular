'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:OverviewCtrl
 * @description
 * # UsersCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('OverviewCtrl', function($scope, $cookieStore,$location,$http) {
	$scope.usernameCookie =  $cookieStore.get("username"); 
    
  });
