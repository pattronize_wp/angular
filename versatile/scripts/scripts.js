 angular
 .module('yapp', [
    'ui.router',
    'snap',
    'ngAnimate',
	'ngCookies'
    ])
 .config(function($stateProvider, $urlRouterProvider) {

   	$urlRouterProvider.when('/dashboard', '/dashboard/overview');
    $urlRouterProvider.otherwise('/login');
    $stateProvider
    .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
    })
    .state('login', {
      url: '/login',
      parent: 'base',
      templateUrl: 'views/login.html',
      controller: 'LoginCtrl'
  	})
    .state('dashboard', {
      url: '/dashboard',
      parent: 'base',
      templateUrl: 'views/dashboard.html',
      controller: 'DashboardCtrl'
 	 })
    .state('overview', {
        url: '/overview',
        parent: 'dashboard',
        templateUrl: 'views/dashboard/overview.html',
		controller: 'OverviewCtrl'
    })
	.state('adduser', {
        url: '/adduser',
        parent: 'base',
        templateUrl: 'views/users/adduser.html'
    })
	.state('addstudent', {
        url: '/addstudent',
        parent: 'base',
        templateUrl: 'views/students/addstudent.html'
    })
	.state('users', {
        url: '/users',
		parent: 'base',
        templateUrl: 'views/users/users.html',
		controller:'UsersCtrl'
    })
	.state('students', {
        url: '/students',
		parent: 'base',
        templateUrl: 'views/students/students.html',
		controller:'UsersCtrl'
    })
    .state('reports', {
        url: '/reports',
        parent: 'dashboard',
        templateUrl: 'views/dashboard/reports.html'
    });

});

/*angular.module("yapp",["ngRoute"])
	.config(function($routeProvider,$locationProvider) {
	 	$locationProvider.html5Mode(true);
        $routeProvider
            .when('/login/', {
                templateUrl : 'views/login.html',
                controller  : 'LoginCtrl'
            })
      
            .when('/dashboard/', {
                templateUrl : 'views/dashboard.html',
                controller  : 'DashboardCtrl'
            })

            .when('/reports/', {
                templateUrl : 'views/dashboard/reports.html',
                controller  : 'reportController'
            })
            .when('/about/', {
                templateUrl : 'views/dashboard/overview.html',
            })
			
            .when('/adduser', {
                templateUrl : 'views/dashboard/adduser.html',
            })
			
			.otherwise({
        		redirectTo: '/login'
      		})	
    });*/
